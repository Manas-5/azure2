from locust import HttpUser, task, between

class MyUser(HttpUser):
    wait_time = between(1, 5)
    host = 'https://manascloud2.azurewebsites.net'  # Update with your Azure Web App URL

    @task
    def numerical_integration_task(self):
        # Use the relative path for the endpoint
        endpoint = '/numericalintegralservice/0/3.14159?N=100'
        
        # Make an HTTP GET request to the specified URL
        response = self.client.get(endpoint)
