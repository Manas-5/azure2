from flask import Flask, request, jsonify
from scipy import integrate
import math

app = Flask(__name__)

def numerical_integration(lower, upper, n):
    # Function to integrate: abs(sin(x))
    integrand = lambda x: abs(math.sin(x))

    # Perform numerical integration using scipy's quad function
    result, _ = integrate.quad(integrand, lower, upper, limit=n)
    return result

@app.route('/numericalintegralservice/<lower>/<upper>', methods=['GET'])
def numerical_integration_service(lower, upper):
    lower = float(lower)
    upper = float(upper)
    
    # List of N values for load testing
    n_values = [10, 100, 100, 1000, 10000, 100000, 1000000]

    results = []

    for n in n_values:
        result = numerical_integration(lower, upper, n)
        results.append({"N": n, "result": result})

    return jsonify(results)

if __name__ == '__main__':
    # Run the Flask app on port 80
    app.run(debug=True, host='0.0.0.0', port=80)
