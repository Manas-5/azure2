# This function is not intended to be invoked directly. Instead it will be
# triggered by an HTTP starter function.
# Before running this sample, please:
# - create a Durable activity function (default name is "Hello")
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
import json

import azure.functions as func
import azure.durable_functions as df
from collections import Counter

def orchestrator_function(context: df.DurableOrchestrationContext):
    appe = []
    input = yield context.call_activity('Input', None)
    Mapper = []
    for file in input:
        Mapper.append(context.call_activity('Mapper', file))
    result = yield context.task_all(Mapper)
    result1 = yield context.call_activity('Shuffler', result)
    reducer = []
    for file2 in result1:
        reducer.append(context.call_activity('Reducer', file2))
    result2 = yield context.task_all(reducer)
    counter = Counter(result2)
    print(counter)
    return counter
main = df.Orchestrator.create(orchestrator_function)