# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import os, uuid
from azure.identity import DefaultAzureCredential
from azure.storage.blob import BlobServiceClient, BlobClient, ContainerClient

def main(name: str) -> list[tuple[int, str]]:
    try:
        print("Azure Blob Storage Python quickstart sample")
        connect_str = 'DefaultEndpointsProtocol=https;AccountName=manas;AccountKey=iKw8PhOaKofluxqe9IRjf4UCluH0pMDIvuc644UEQyjkEXmaz+jjxB/VqjqXu671svLccRyrTfCf+AStPFBnZQ==;EndpointSuffix=core.windows.net'
        blob_service_client = BlobServiceClient.from_connection_string(connect_str)
        blob_client = blob_service_client.get_container_client(container="manascon")
        blob_list = blob_client.list_blobs()

    except Exception as ex:
        print('Exception:')
        print(ex)
    
    empty_list = []
    for blob in blob_list:
        file = blob_client.download_blob(blob.name).content_as_text()
        val = file.splitlines()
        offset = list(range(1, len(val)))
        empty_list = empty_list + list(zip(offset, val))

    return empty_list  