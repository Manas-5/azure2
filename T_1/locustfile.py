from locust import HttpUser, task, between

class MyUser(HttpUser):
    wait_time = between(1, 5)
    host="http://127.0.0.1:5000/"
    @task
    def numerical_integration_task(self):
        self.client.get('/numericalintegralservice/0/3.14159?N=100')
