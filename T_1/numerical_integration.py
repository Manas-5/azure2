import math

def numerical_integration(lower, upper, N):
    delta_x = (upper - lower) / N
    result = 0

    for i in range(N):
        x_i = lower + i * delta_x
        result += abs(math.sin(x_i)) * delta_x

    return result

if __name__ == "__main__":
    intervals = [10, 100, 100, 1000, 10000, 100000, 1000000]
    lower_limit, upper_limit = 0, 3.14159

    results = [numerical_integration(lower_limit, upper_limit, N) for N in intervals]

    for N, result in zip(intervals, results):
        print(f"N={N}: {result}")
