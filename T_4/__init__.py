import logging
import azure.functions as func
import numpy as np
from scipy import integrate
import json
import math

def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    lower = float(req.params.get('lower', 0))  # Get 'lower' parameter from the query string, default to 0
    upper = float(req.params.get('upper', math.pi))  # Get 'upper' parameter from the query string, default to pi

    # List of N values for load testing
    n_values = [10, 100, 1000, 10000, 100000, 1000000]

    results = []

    for n in n_values:
        result = numerical_integration(lower, upper, n)
        results.append({"N": n, "result": result})

    return func.HttpResponse(json.dumps(results), mimetype='application/json', status_code=200)


def numerical_integration(lower, upper, n):
    # Function to integrate: abs(sin(x))
    integrand = lambda x: abs(np.sin(x))

    # Create an array of sample points
    x_values = np.linspace(lower, upper, n)

    # Calculate the area using the trapezoidal rule
    result = np.trapz(integrand(x_values), x_values)

    return result
