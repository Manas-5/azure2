from locust import HttpUser, task, between

class MyUser(HttpUser):
    wait_time = between(1, 2)

    @task
    def my_task(self):
        response = self.client.get("https://intf.azurewebsites.net/api/httptrigger1?lower=0&upper=3.14159")
        print(response.text)
