# DO NOT include azure-functions-worker in this file
# The Python Worker is managed by Azure Functions platform
# Manually managing azure-functions-worker may cause unexpected issues

azure-functions
numpy==1.21.2
scipy==1.7.1
Flask==3.0.0
